const express = require('express');
const router = express.Router();
const {Tarea} = require('../models/Tarea');



//GET: listado de tareas- http://localhost:4000/api/tareas
router.get('/api/tareas',async(req, res)=> {

    const tareas = await Tarea.find();

    res.send(tareas);
});
//GET: una tarea determinada- http://localhost:4000/api/tareas/:id
router.get('/api/tareas/:id',(req, res)=> {
    res.send([{nombre: "tarea 1"}]);
});

//POST: crear una tarea - http://localhost:4000/api/tareas
router.post('/api/tareas', async (req, res)=>{

    try {
        const {nombre, descripcion, finalizada} = req.body;    

    const tarea = new Tarea({
        nombre,
        descripcion,
        finalizada
    });

    let nuevaTarea = await tarea.save();
    res.status(201).send(nuevaTarea);
    } catch (error) {
        console.log(error);
        res.status(500).send({mensaje: 'Error desconocido. Contactarse con soporte'});
    }
});

//PUT: actualizar una tarea - http://localhost:4000/api/tareas
//DELETE: eliminar una tarea determinada - http://localhost:4000/api/tareas/:id
//PUT: cambiar estado de una tarea
//GET: buscar tareas por algun criterio

module.exports = router;