//imports
const express = require('express');
const app = express();
const mongoose = require('mongoose');

const tareasRouter = require('./routes/tareasRoutes');
//configurations
const PORT= 4000;
const MONGO_URI = 'mongodb://localhost:27017/programacion3-2020';

mongoose.connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
//middelwares
app.use(express.json());

//routs to ours services
app.use(tareasRouter);
//start app
app.listen(PORT, ()=> console.log(`iniciando aplicacion en ${PORT}`));
