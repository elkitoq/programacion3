
**1. ¿Qué es git?**
Git es un sistema de control de versiones.

**2. ¿Por qué queremos utilizar git?**
Por qué es la forma más fácil de manejar versiones y permitir colaboración de otros desarrolladores de forma remota

**3. ¿Qué es el bash que instala git?**
Es una herramienta de tipo consola que te permite manipular y gestionar todo el proceso a realizar con el proyecto

**4. Describa los estados (working directory, staging area, repository)**
*Working Directory*: Es donde podemos hacer cualquier cambio y no afectar nuestro repositorio en lo absoluto
*Staging Area*: Aqui le podemos dar nombre a nuestra nueva versión, y le cambiamos el estado de modificado a preparado mediante el comando git add . , para pasar el estado de preparado a confirmado y enviarlo al repositorio lo hacemos mediante el comando git commit -m "nombre de la version"
*Repository*: Una vez que el código esta confirmado ya esta listo para sincronizarse con el servidor de Git (github, Gitlab,etc). Para hacer esto, escribimos el siguiente comando git push 

**5. Describa el flujo para crear un nuevo repositorio git.**
Podemos crear el proyecto en la pagina de gitlab, y luego clonarlo en nuestro pc,en caso de hacerlo así, una vez creado el proyecto , creamos una carpeta donde clonaremos el repositorio, vamos al símbolo de sistema y nos ubicamos en la carpeta creada, luego utilizamos el comando git clone https//gitlab.com/la-dirección/correspondiente.

**6. Describa el flujo para agregar un archivo simple al repositorio.**
Utilizaremos los comandos:
git add .
git commit -m "Nombre"

**7. Describa el flujo para cambiar el archivo agregado y guardar los cambios en el repositorio.**
git add archivo.simple
git commit -m "Modificando un archivo simple"

**8. ¿Cómo hago para ignorar un archivo o carpeta?**
Utilizando el comando .gitignore: y colocando el nombre de lo que queremos ignorar

**9. Explique qué es un branch. Dé un pequeño ejemplo de cómo haría uno.**
Son espacios o entornos independientes para que se pueda usar y así trabajar sobre un mismo Proyecto sin borrar el conjunto de archivos originales del proyecto, dándonos flexibilidad para desarrollar nuestro proyecto de manera más organizada.

usamos el comando git branch ejemplo

**10. ¿Qué hago con `git add .`?**
Este comando añade al índice cualquier fichero nuevo o que haya sido modificado

**11. ¿Cómo cambiamos de un branch a otro?**
usando el comando git checkout "nombre_rama"