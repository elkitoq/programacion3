function leerCookie(nombre) {
    var lista = document.cookie.split(";");
    var micookie = "=";
    for (i in lista) {
        var busca = lista[i].search(nombre);
        if (busca > -1) {micookie=lista[i]}
        }
    var igual = micookie.indexOf("=");
    var valor = micookie.substring(igual+1);
    return valor;
    }

    

window.onload = function(){
    var carrito = decodeURIComponent(leerCookie("carrito"));
    var tabla = document.querySelector("tbody");
    tabla.innerHTML=carrito;

    const productos = document.getElementsByClassName('borrar-producto');

    function cargarEventos(){
        console.log(productos);
        for(producto of productos){
            producto.addEventListener('click', (e)=>{borrar(e)});
        }
        
    }

    function borrar(e){
        e.preventDefault();
        const producto = e.target.parentElement.parentElement;
        console.log(producto);
        producto.outerHTML="";
        createCookie("carrito",encodeURIComponent(tabla.innerHTML),1);
        total();
    }   

    function createCookie(name, value, days) {
        var expires = '',
            date = new Date();
        if (days) {
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = '; expires=' + date.toGMTString();
        }
        console.log(document.cookie = name + '=' + value + expires + '; path=/');
    }

    function total(){
        var precios = tabla.getElementsByClassName('carrito-precio');
        var total = 0;
        for(precio of precios){
            total += parseInt(precio.innerHTML);
        }
        document.getElementById(`total`).innerHTML=total;

    }



    cargarEventos();
    total();
}
