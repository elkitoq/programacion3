
window.onload = function(){

    class Carrito{

        compraProducto(e){
            console.log(e.target);
            e.preventDefault();
            if(e.target.classList.contains('agregar-carrito')){
                const producto = e.target.parentElement.parentElement.parentElement;
                this.leerDatosProducto(producto);
            }
            else if(e.target.classList.contains('bi')){
              const producto = e.target.parentElement.parentElement.parentElement.parentElement;
              this.leerDatosProducto(producto);
            }
            else{
              const producto = e.target.parentElement.parentElement.parentElement.parentElement.parentElement;
              this.leerDatosProducto(producto);
            }
        }

        leerDatosProducto(producto){
          console.log(producto);
            const infoProducto = {
                imagen : producto.querySelector("img").src,
                titulo : producto.querySelector('h3').textContent,
                descripcion : producto.querySelector('#descripcion').textContent,
                precio : producto.querySelector('#precio').textContent,
                cantidad : 1
            }
            this.insertarCarrito(infoProducto);
        }

        insertarCarrito(producto){
            const row = document.createElement('tr');
            row.innerHTML= `
                <td>
                    <img src="${producto.imagen}" width= 100>
                </td>
                <td>${producto.titulo}</td>
                <td>${producto.descripcion}</td>
                <td class="carrito-precio">${producto.precio}</td>
                <td>
                    <a href="#" class="borrar-producto fas fa-times-circle"></a>
                </td>
            `;
            var cookie= leerCookie("carrito");
            cookie+=encodeURIComponent(row.outerHTML);
            createCookie("carrito",cookie,1);
            
            
            //listaProductos.appendChild(row);
        }
    }


    const carro = new Carrito();
    const carrito = document.getElementById('container-carrito');
    const productos = document.getElementById('lista-productos');
    //const listaProductos = document.querySelector('#lista-carrito > tbody');

    cargarEventos();

    function cargarEventos(){
        productos.addEventListener('click', (e)=>{carro.compraProducto(e)});
    }

    function createCookie(name, value, days) {
        var expires = '',
            date = new Date();
        if (days) {
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = '; expires=' + date.toGMTString();
        }
        console.log(document.cookie = name + '=' + value + expires + '; path=/');
    }

    function leerCookie(nombre) {
        var lista = document.cookie.split(";");
        var micookie = "=";
        for (i in lista) {
            var busca = lista[i].search(nombre);
            if (busca > -1) {micookie=lista[i]}
            }
        var igual = micookie.indexOf("=");
        var valor = micookie.substring(igual+1);
        return valor;
        }
}



